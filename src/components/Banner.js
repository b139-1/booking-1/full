import {Container, Row, Col} from "react-bootstrap"
import {Link} from "react-router-dom"

export default function Banner({bannerProp}) {
	
	const {title, message, option, destination} = bannerProp

	return (
		<Container fluid className="m-4">
			<Row>
				<Col>
					<div className="jumbotron">
						<h1>{title}</h1>
						<p>{message}</p>
						<Link className="btn btn-primary" to={destination}>{option}</Link>
					</div>
				</Col>
			</Row>
		</Container>
	)
}