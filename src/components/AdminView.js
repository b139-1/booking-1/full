import {Container, Button, Row, Col, Table, Modal, Form} from "react-bootstrap"
import {useState, useEffect, Fragment} from "react"
import Swal from "sweetalert2"

export default function AdminView(props) {

	const {courseData, fetchData} = props

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [courses, setCourses] = useState([])
	const [showAdd, setShowAdd] = useState(false)
	const [courseId, setCourseId] = useState("")

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	useEffect(() => {
		const courseArr = courseData.map(course => {
			setCourseId(course._id)
			return (
				<tr>
					<td>{course.courseName}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
						{
							(course.isActive === true) ?
								<span className="text-success">Available</span>
							:
								<span className="text-danger">Unavailable</span>
						}
					</td>
					<td>
						<Button onClick={() => openEdit(course._id)}>Update</Button>
						{
							(course.isActive)?
							 	<Fragment>
							 		<Button variant="secondary" onClick={() => archiveCourse(course._id, course.isActive)} className="m-2">Disable</Button>
							 		<Button variant="danger" onClick={() => deleteCourse(course._id)} className="m-2">Delete</Button>
							 	</Fragment>
							:
								<Fragment>
									<Button variant="success" onClick={() => unarchiveCourse(course._id, course.isActive)} className="m-2">Enable</Button>
									<Button variant="danger" onClick={() => deleteCourse(course._id)} className="m-2">Delete</Button>
								</Fragment>
						}
					</td>
				</tr>
			)
		})
		setCourses(courseArr)
	}, [courseData])

	const addCourse = (event) => {
		event.preventDefault()
		fetch("http://localhost:4000/api/courses/create-course", {
			method: "POST", 
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "You have added a new course!"
				})
				setName("")
				setDescription("")
				setPrice(0)
				closeAdd()
				fetchData()
			} else {
				Swal.fire({
					title: "Oh no!",
					icon: "error",
					text: "Something went wrong, try again."
				})
				fetchData()
			}
		})
		closeAdd()
	}

	const editCourse = (event, courseId) => {
		event.preventDefault()
		fetch(`http://localhost:4000/api/courses/${courseId}/edit`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(typeof data !== "undefined") {
				fetchData()
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Course edited!"
				})
				closeAdd()
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong, try again."
				})
				fetchData()
			}
		})
	}

	const openEdit = (courseId) => {
		openAdd()
		fetch(`http://localhost:4000/api/courses/find-course/${courseId}`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setName(data.courseName)
			setDescription(data.description)
			setPrice(data.price)
		})
		setShowAdd(true)
	}

	const archiveCourse = (courseId, isActive) => {
		fetch(`http://localhost:4000/api/courses/archive-courses/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			if(data === true){

				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course disabled"
				})
			} else {
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const unarchiveCourse = (courseId, isActive) => {
		fetch(`http://localhost:4000/api/courses/unarchive-courses/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true){

				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course enabled!"
				})
			} else {
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const deleteCourse = (courseId) => {
		fetch(`http://localhost:4000/api/courses/delete-course/${courseId}`, {
			method: "DELETE",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data === true) {
				fetchData()
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Course deleted!"
				})
			} else {
				fetchData()
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return (
		<Container className="m-3">
			<h2 className="text-center">Admin Dashboard</h2>
			<Row className="justify-content-center">
				<Col>
					<div className="text-right">
						<Button onClick={openAdd}>Add New Course</Button>
					</div>
				</Col>
			</Row>
			<Table striped border hover className="m-3">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
			</Table>

			<Modal show={showAdd} onHide={closeAdd}>
				<Modal.Header closeButton>
					<Modal.Title>Add Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={(e) => addCourse(e)}>
						<Form.Group controlId="courseName">
							<Form.Label>Course Name:</Form.Label>
							<Form.Control 
							type="text" 
							value={name} 
							onChange={(event) => setName(event.target.value)}/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Decription:</Form.Label>
							<Form.Control 
							type="text" 
							value={description} 
							onChange={(event) => setDescription(event.target.value)}/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Course Price:</Form.Label>
							<Form.Control 
							type="number" 
							value={price} 
							onChange={(event) => setPrice(event.target.value)}/>
						</Form.Group>
						<div className="text-right">
							<Button 
								variant="secondary" 
								onClick={closeAdd} 
								className="m-2">
								Cancel
							</Button>
							<Button 
								variant="success" 
								type="submit" 
								className="m-2">
								Save Changes
							</Button>
						</div>
					</Form>
				</Modal.Body>
			</Modal>
			
			<Modal show={showAdd} onHide={closeAdd}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={(e) => editCourse(e, courseId)}>
						<Form.Group controlId="courseName">
							<Form.Label>Course Name:</Form.Label>
							<Form.Control 
							type="text" 
							value={name} 
							onChange={(event) => setName(event.target.value)}/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Decription:</Form.Label>
							<Form.Control 
							type="text" 
							value={description} 
							onChange={(event) => setDescription(event.target.value)}/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Course Price:</Form.Label>
							<Form.Control 
							type="number" 
							value={price} 
							onChange={(event) => setPrice(event.target.value)}/>
						</Form.Group>
						<div className="text-right">
							<Button 
								variant="secondary" 
								onClick={closeAdd} 
								className="m-2">
								Cancel
							</Button>
							<Button 
								variant="success" 
								type="submit" 
								className="m-2">
								Save Changes
							</Button>
						</div>
					</Form>
				</Modal.Body>
			</Modal>
		</Container>
	)
}