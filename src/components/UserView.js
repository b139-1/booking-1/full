import CourseCard from "./CourseCard.js"
import {useState, useEffect, Fragment} from "react"

export default function UserView({courseData}) {
	
	const [courses, setCourses] = useState([])

	useEffect(() => {
		const courseArray = courseData.map(course => {
			if(course.isActive === true) {
				return(
					<CourseCard 
						courseProp={course} 
						key={course._id} 
					/>
				)
			} else {
				return null
			}			
		})
		setCourses(courseArray)
	}, [courseData])
	//console.log(courses)	
	return (
		//<h1>Hello from UserView component</h1>
		<Fragment>
			{courses}
		</Fragment>
	)
}