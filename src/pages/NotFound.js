import Banner from "./../components/Banner.js"
import message from "./../data/message.js"

export default function NotFound() {
	return (
		<Banner bannerProp={message[1]}/>
	)
}