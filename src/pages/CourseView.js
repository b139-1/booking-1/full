import {Container, Row, Col, Card, Button} from "react-bootstrap"
import {useEffect, useState, useContext} from "react"
import {useParams, useHistory, Link} from "react-router-dom"
import Swal from "sweetalert2"
import UserContext from "./../UserContext.js"

export default function CourseView() {

	const {courseId} = useParams()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const {user} = useContext(UserContext)
	// console.log(user)

	let history = useHistory()

	useEffect(() => {
		fetch(`http://localhost:4000/api/courses/find-course/${courseId}`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setName(data.courseName)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [courseId])

	const enroll = (courseId) => {
		fetch("http://localhost:4000/api/users/enroll", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}, 
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: "Succesfully Enrolled!",
					icon: "success",
					text: "You have been enrolled for this course."
				})
				history.push("/courses")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	return(
		<Container>
			<Row className="m-3 justify-content-center">
				<Col xs={8} md={4}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Course Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							<Card.Text>
								Class Schedule:
								<p>8:00 am - 5:00 pm</p>
							</Card.Text>
							{
								(user.id !== null) ? 
									<Button onClick={() => enroll(courseId)}>Enroll</Button>
								:
									<Link className="btn btn-primary" to="/login">Login to enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}