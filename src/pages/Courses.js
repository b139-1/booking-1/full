import {Fragment, useEffect, useState, useContext} from "react"
import UserContext from "./../UserContext.js"
import courseData from "./../data/courseData.js"
import CourseCard from "./../components/CourseCard.js"
import AdminView from "./../components/AdminView.js"
import UserView from "./../components/UserView.js"

export default function Courses() {

	const [courses, setCourses] = useState([])
	const {user} = useContext(UserContext)
	// console.log(user)

	const fetchData = () => {
		fetch("http://localhost:4000/api/courses/")
		.then(res => res.json())
		.then(data => {
			setCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])
	// console.log(user.isAdmin)
	return (
		<Fragment>
			{
				(user.isAdmin === true) ?
					<AdminView courseData={courses} fetchData={fetchData}/>
				:
					<UserView courseData={courses}/>
			}
		</Fragment>
	)
}