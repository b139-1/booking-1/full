// import from sweetalert2
import Swal from "sweetalert2"
// import from react
import {Container, Form, Button} from "react-bootstrap"
import {useState, useEffect, useContext, Fragment} from "react"
import {Redirect, BrowserRouter, Switch, Route} from "react-router-dom"
// useContext is used to unpack the data from the UserContext
import UserContext from "./../UserContext.js"
import Courses from "./../pages/Courses.js"

export default function Login() {
	
	const {user, setUser} = useContext(UserContext)
	const [logEmail, setLogEmail] = useState("")
	const [logPassword, setLogPassword] = useState("")
	const [isDisabled, setIsDisabled] = useState(false)
	
	useEffect(() => {
			if(logEmail !== '' && logPassword !== '') {
				setIsDisabled(false)
			} else {
				setIsDisabled(true)
			}
	}, [logEmail, logPassword])
	
	const login = (event) => {
			event.preventDefault()
			fetch("http://localhost:4000/api/users/login", {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: logEmail,
					password: logPassword
				})
			})
			.then(res => res.json())
			.then(data => {
				if(typeof data !== "undefined") {
					// store data in local storage
					localStorage.setItem("token", data.access)
					userDetails(data.access)
					// alert the user that login is successful
					Swal.fire({
						title: "Login succesful!", 
						icon: "success",
						text: "Welcome to Course Booking"
					})
				} else {
					Swal.fire({
						title: "Authentication failed!", 
						icon: "error",
						text: "Check login details and try again."
					})
				}
			})

			setLogEmail("")
			setLogPassword("")
	}

	const userDetails = (token) => {

		// send request to the server
		fetch("http://localhost:4000/api/users/details", {
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
	return (
		(user.id !== null) ?
			<Redirect to="courses"/>
		:
			<Container>
				<Form className="border p-3 m-4" onSubmit={(event) => login(event)}>
				  <Form.Group className="mb-3" controlId="logEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control type="email" placeholder="Enter email" value={logEmail} onChange={(event) => setLogEmail(event.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="logPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" placeholder="Password" value={logPassword} onChange={(event) => setLogPassword(event.target.value)}/>
				  </Form.Group>
				  <Button variant="success" type="submit" disabled={isDisabled}>
				    Login
				  </Button>
				</Form>
			</Container>			
	)
}